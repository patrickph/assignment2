# Assignment 2
## Trivia Game
 A minimalistic approach to your classic trivia game.

 Hosted on Heroku:
 https://desolate-fjord-22544.herokuapp.com/

## Dependencies
- Node.js
- npm

## Project setup
```bash
npm install
```

### Compiles and hot-reloads for development
```bash
npm run serve
```

### Compiles and minifies for production
```bash
npm run build
```

### Lints and fixes files
```bash
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
