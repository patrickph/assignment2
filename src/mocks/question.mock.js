export const MOCK_QUESTION_EASY_CATEGORY_9 = {
    "response_code":0,
    "results":
    [
        {
            "category":"General Knowledge",
            "type":"multiple",
            "difficulty":"easy",
            "question":"What is the first book of the Old Testament?",
            "correct_answer":"Genesis",
            "incorrect_answers":[
                "Exodus",
                "Leviticus",
                "Numbers"
            ]
        },
        {
            "category":"General Knowledge",
            "type":"multiple",
            "difficulty":"easy",
            "question":"What does a funambulist walk on?",
            "correct_answer":"A Tight Rope",
            "incorrect_answers":
            [
                "Broken Glass",
                "Balls",
                "The Moon"
            ]
        },
        {
            "category":"General Knowledge",
            "type":"boolean",
            "difficulty":"easy",
            "question":"Video streaming website YouTube was purchased in it&#039;s entirety by Facebook for US$1.65 billion in stock.",
            "correct_answer":"False",
            "incorrect_answers":
            [
                "True"
            ]
        },
        {
            "category":"General Knowledge",
            "type":"multiple",
            "difficulty":"easy",
            "question":"What word represents the letter &#039;T&#039; in the NATO phonetic alphabet?",
            "correct_answer":"Tango",
            "incorrect_answers":
            [
                "Target",
                "Taxi",
                "Turkey"
            ]
        },
        {
            "category":"General Knowledge",
            "type":"boolean",
            "difficulty":"easy",
            "question":"A scientific study on peanuts in bars found traces of over 100 unique specimens of urine.",
            "correct_answer":"False",
            "incorrect_answers":
            [
                "True"
            ]
        },
        {
            "category":"General Knowledge",
            "type":"multiple",
            "difficulty":"easy",
            "question":"Which American-owned brewery led the country in sales by volume in 2015?",
            "correct_answer":"D. G. Yuengling and Son, Inc",
            "incorrect_answers":
            [
                "Anheuser Busch",
                "Boston Beer Company",
                "Miller Coors"
            ]
        },
        {
            "category":"General Knowledge",
            "type":"boolean",
            "difficulty":"easy",
            "question":"Scotland voted to become an independent country during the referendum from September 2014.",
            "correct_answer":"False",
            "incorrect_answers":
            [
                "True"
            ]
        }
    ]
}