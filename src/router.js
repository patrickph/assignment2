import VueRouter from 'vue-router'
import Start from './components/Start/Start.vue'
import Questions from './components/Questions/Questions.vue'
import Results from './components/Results/Results.vue'

const routes = [
    {
        path: '/start',
        name: 'Start',
        component: Start,
        alias: '/'
    },
    {
        path: '/questions',
        name: 'Questions',
        component: Questions
    },
    {
        path: '/results',
        name: 'Results',
        component: Results,
    }
];

const router = new VueRouter({ routes });

export default router;