

const baseURL = "https://opentdb.com/";

export function fetchSessionToken() {
    return fetch(`${baseURL}api_token.php?command=request`)
}

export function fetchCategories() {
    return fetch(`${baseURL}api_category.php`)
        .then(res => res.json())
        .then(categories => categories.trivia_categories)
}

export function fetchNoQuestions(category) {
    return fetch(`${baseURL}api_count.php?category=${category}`)
}

export function fetchQuestions(amount, category, difficulty) {
    return fetch(`${baseURL}api.php?amount=${amount}&category=${category}&difficulty=${difficulty}`)
        .then(res => res.json())
}